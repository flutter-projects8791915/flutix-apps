Selamat datang di Flutix Project.

Project ini menggunakan bahasa pemrograman Dart dan Framework Flutter.

Fitur-fitur yang ada di dalam project ini :

1. State Management menggunakan Bloc Provider (https://pub.dev/packages/flutter_bloc)
2. Menggunakan design pattern MVVM (https://www.dicoding.com/blog/tips-design-pattern-mvvm/)
3. Data Film di ambil dari Source Open API TMDB (https://www.themoviedb.org/)
4. Add Money e-wallet
5. Booking film untuk jadwal yang akan datang
6. dan masih banyak lainya

Silahkan clone dan test running untuk melihat lebih detail.

Terima Kasih