part of 'models.dart';

class Theater extends Equatable {
  final String name;

  Theater(this.name);

  @override
  List<Object> get props => [name];
}

List<Theater> dummyTheaters = [
  Theater("CGV Blok M Plaza"),
  Theater("CGV Cinere Mall"),
  Theater("XXI Gandaria City"),
  Theater("XXI Senayan City")
];
